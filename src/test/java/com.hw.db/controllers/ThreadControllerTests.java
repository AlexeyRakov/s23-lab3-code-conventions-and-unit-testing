package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadControllerTests {
    ThreadController threadController;
    User user;
    Post post;
    ArrayList<Post> posts;
    Thread thread;
    ArrayList<Thread> threads;
    int id = 1;
    String forum = "forum123";
    String slug = "slug123";
    String nickname = "SomeName";


    @BeforeEach
    void setUp() {
        threadController = new ThreadController();
        Timestamp timestamp = new Timestamp(12345678);
        user = new User(nickname, "somename@gmail.com", "SomeName AnotherSomeName", "Some info");

        post = new Post(nickname, timestamp, forum, "Hehe", 0, 0, false);
        posts = new ArrayList<>();
        posts.add(post);

        thread = new Thread(nickname, timestamp, forum, "Some message in some new thread", slug, "Some new thread", 0);
        thread.setId(id);
        threads = new ArrayList<>();
        threads.add(thread);
    }


    @Test
    @DisplayName("Test - Check ID and Slug")
    void testCheck() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadById(id)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(thread, controller.CheckIdOrSlug(((Integer) id).toString()), "Check id successful");
            assertEquals(thread, controller.CheckIdOrSlug(slug), "Check slug successful");
        }
    }

    @Test
    @DisplayName("Test - Thread Post Create")
    void testCreate() {
        List<Post> threadPosts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(threadPosts),
                    controller.createPost(slug, threadPosts), "Thread post create successful");
        }
    }

    @Test
    @DisplayName("Test - Thread Posts Get")
    void testThreadPostGet() {
        List<Post> threadPosts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 200, 2, null, false)).thenReturn(threadPosts);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(threadPosts),
                    controller.Posts(slug, 200, 2, null, false), "Thread Posts successful");
        }
    }

    @Test
    @DisplayName("Test - Thread Change")
    void testChange() {
        Thread changeThread = new Thread("SomePerson", new Timestamp(2), "SomeForum", "SomeMassage", "newSlug123", "SomeNewForum", 123);
        changeThread.setId(2);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("newSlug123")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.change("newSlug123", thread),
                    "Change successful");
        }
    }

    @Test
    @DisplayName("Test - Thread Information")
    void testInformation() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(slug),
                    "Information successful");
        }
    }

    @Test
    @DisplayName("Test - Create Vote")
    public void testCreateVote() {
        try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
            try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                mockedUserDAO.when(() -> UserDAO.Info(nickname)).thenReturn(user);
                Vote vote = new Vote(nickname, 1);
                mockedThreadDAO.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.createVote(slug, vote), "Create Vote successful");
            }
        }
    }

}
